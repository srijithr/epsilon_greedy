
### About

This repository contains notebooks for testing out Multiarm Bandit algorithms. The following notebooks are relevant for this purpose:

1. Epsilongreedy.ipynb     - Implements A/B test, Epsilongreedy and Epsilongreedy with decay
2. Contextualbandits.ipynb - Implements Epsilon greedy and contextual epsilon greedy
3. Thompsonsampling.ipynb  - Implements the contextual Thompson sampling algorithm 
